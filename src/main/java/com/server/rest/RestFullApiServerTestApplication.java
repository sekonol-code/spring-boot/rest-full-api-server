package com.server.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestFullApiServerTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestFullApiServerTestApplication.class, args);
	}

}

