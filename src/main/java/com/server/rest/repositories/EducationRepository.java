package com.server.rest.repositories;

import com.server.rest.entities.Educations;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * EducationRepository
 */
public interface EducationRepository extends JpaRepository<Educations, Integer> {

    // List<Educations> findAllById_ap(int id_ap);

}