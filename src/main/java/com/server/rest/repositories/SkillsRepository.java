package com.server.rest.repositories;

import com.server.rest.entities.Skills;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * SkillsRepository
 */
public interface SkillsRepository extends JpaRepository<Skills, Integer> {

    // void findAllByIdAp(int id_ap);

}