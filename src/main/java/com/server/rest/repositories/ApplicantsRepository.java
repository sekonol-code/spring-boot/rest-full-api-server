package com.server.rest.repositories;

import com.server.rest.entities.*;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * AplicantsDao
 */
public interface ApplicantsRepository extends JpaRepository<Applicants, Integer>, ApplicantsDao {

}
