package com.server.rest.controller;

import org.springframework.web.bind.annotation.RestController;

import com.server.rest.entities.Applicants;
import com.server.rest.services.ApplicantsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * AplicantsController
 */
@RestController
public class ApplicantsController {

    @Autowired
    private ApplicantsService applicantsService;

    @RequestMapping(value = "/path", method = RequestMethod.GET)
    public Page<Applicants> requestMethodName(Pageable pageable) {
        return applicantsService.joinAplicants(pageable);
    }

}