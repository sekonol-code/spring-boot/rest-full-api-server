package com.server.rest.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * Educations
 */

@Entity
@Data
@RequiredArgsConstructor
@AllArgsConstructor

public class Educations {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_education;

    @NotEmpty
    @NotNull
    private int id_ap;

    @NotEmpty
    private Date date_awal;

    @NotEmpty
    private Date date_akhir;

    @NotEmpty
    @NotNull
    private String school;

    @NotEmpty
    @NotNull
    private String major;

    private String description;
}