package com.server.rest.entities;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class Applicants {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_ap;

    @NotEmpty
    @NotNull
    private String nama;

    private String alamat;

    @NotEmpty
    @NotNull
    private String email;

    @NotEmpty
    @NotNull
    private String contact;

    private String personal_statement;

    private byte[] data_gambar;

    private String link_gambar;

    private byte[] data_cv;

    private String link_cv;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_ap")
    private List<Educations> educations;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_ap")
    private List<Skills> skills;
}