package com.server.rest.services;

import com.server.rest.entities.Applicants;
import com.server.rest.repositories.ApplicantsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * AplicantsDto
 */
@Service
public class ApplicantsDto implements ApplicantsService {

    @Autowired
    ApplicantsRepository applicantsRepository;


    @Override
    @Transactional
    public Page<Applicants> joinAplicants(Pageable pageable) {
        return applicantsRepository.findAll(pageable);
    }

}