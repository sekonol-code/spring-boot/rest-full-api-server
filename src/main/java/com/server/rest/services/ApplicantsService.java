package com.server.rest.services;

import com.server.rest.entities.Applicants;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

/**
 * AplicantsService
 */
@Component
public interface ApplicantsService {

    public Page<Applicants> joinAplicants(Pageable pageable);

}